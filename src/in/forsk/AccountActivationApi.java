package in.forsk;

import in.forsk.utils.HttpUtils;
import in.forsk.utils.ReponseUtils;

import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;
import java.util.Random;
import java.util.UUID;

import org.joda.time.Period;
import org.json.JSONException;
import org.json.JSONObject;

import com.shephertz.app42.paas.customcode.Executor;
import com.shephertz.app42.paas.customcode.HttpRequestObject;
import com.shephertz.app42.paas.customcode.HttpResponseObject;
import com.shephertz.app42.paas.sdk.java.ServiceAPI;
import com.shephertz.app42.paas.sdk.java.storage.Storage;
import com.shephertz.app42.paas.sdk.java.storage.StorageService;

public class AccountActivationApi implements Executor {

	HttpResponseObject res;

	@Override
	public HttpResponseObject execute(HttpRequestObject req) {

		String body = req.getUri();
		try {

			if (body != null && body.length() > 0) {

				Map<String, String> map = HttpUtils.splitQuery(new URL("http://api-engine.shephertz.com/" + req.getUri()));

				ServiceAPI serviceAPI = new ServiceAPI("b0ae16872b9cec26214e6182c1022a28fa08bb1abc8a77f987fd91f0d85ca60d", "ab810bf7fca1ba3d75eaee6798f3b9b79eb2068193d0f6e4306c9b61d49a6765");

				StorageService storageService = serviceAPI.buildStorageService();

				String activation_key = map.get("activation_key");
				String otp = map.get("otp");

				String response_message = "";

				if (activation_key != null && activation_key.length() > 0) {
					Storage storage_rule = storageService.findDocumentByKeyValue(GlobalConstant.DB_NAME, GlobalConstant.CollectionName.USER_PROPERTY, "activation_key", activation_key);
					ArrayList<Storage.JSONDocument> jsonDocList = storage_rule.getJsonDocList();

					if (jsonDocList.size() > 0) {

						JSONObject user_property = new JSONObject(jsonDocList.get(jsonDocList.size() - 1).getJsonDoc());

						String key_created_date = user_property.getString("key_created_date");

						String phone = user_property.getString("phone");

						DateFormat dateFormat = new SimpleDateFormat(GlobalConstant.DATE_FORMATE);

						Date date = new Date();

						Date key_date = dateFormat.parse(key_created_date);

						// res = new HttpResponseObject(200,
						// ""+key_date.toString(), null);
						// return res;

						Period p = new Period(date.getTime(), key_date.getTime());
						int hours = p.getHours();

						if (hours < GlobalConstant.LINK_EXPIRE_OFFSET) {

							user_property.put("activation_key", UUID.randomUUID().toString().replaceAll("-", ""));
							user_property.put("forgetpass_key", UUID.randomUUID().toString().replaceAll("-", ""));
							user_property.put("is_email_verified", 1);
							user_property.put("email_verification_time", dateFormat.format(date));

							storageService.updateDocumentByKeyValue(GlobalConstant.DB_NAME, GlobalConstant.CollectionName.USER_PROPERTY, "phone", phone, user_property);

							response_message = "Email Verified";
						} else {
							// Link expire
							response_message = "Email Verification expair";
							res = new HttpResponseObject(200, ReponseUtils.getErrorResponse(response_message), null);
							return res;
						}
					}
				}

				if (otp != null && otp.length() > 0) {

				}
				Storage storage_rule = storageService.findDocumentByKeyValue(GlobalConstant.DB_NAME, GlobalConstant.CollectionName.USER_PROPERTY, "otp", otp);
				ArrayList<Storage.JSONDocument> jsonDocList = storage_rule.getJsonDocList();

				if (jsonDocList.size() > 0) {
					JSONObject user_property = new JSONObject(jsonDocList.get(jsonDocList.size() - 1).getJsonDoc());

					String key_created_date = user_property.getString("key_created_date");

					String phone = user_property.getString("phone");

					DateFormat dateFormat = new SimpleDateFormat(GlobalConstant.DATE_FORMATE);

					Date date = new Date();

					Date key_date = dateFormat.parse(key_created_date);

					Period p = new Period(date.getTime(), key_date.getTime());
					int hours = p.getHours();

					if (hours < GlobalConstant.LINK_EXPIRE_OFFSET) {

						user_property.put("otp", "" + gen());
						user_property.put("is_mobile_verified", 1);
						user_property.put("mobile_verification_time", dateFormat.format(date));

						storageService.updateDocumentByKeyValue(GlobalConstant.DB_NAME, GlobalConstant.CollectionName.USER_PROPERTY, "phone", phone, user_property);

						response_message = "/Mobile Verified";

					} else {
						// Link expire
						response_message = "/Mobile Verification expair";

						res = new HttpResponseObject(200, ReponseUtils.getErrorResponse(response_message), null);
						return res;
					}
				}

				res = new HttpResponseObject(200, ReponseUtils.getSuccessResponse(response_message), null);

			} else {
				res = new HttpResponseObject(200, "Empty Body", null);

			}
		} catch (Exception e) {
			e.printStackTrace();
			res = new HttpResponseObject(500, "Invalid Activation link", null);

		}

		return res;
	}

	public String getString(JSONObject jObject, String key) {
		try {
			return jObject.getString(key);
		} catch (JSONException e) {
			return "";
		}
	}

	public int gen() {
		Random r = new Random(System.currentTimeMillis());
		return ((1 + r.nextInt(2)) * 10000 + r.nextInt(10000));
	}

	public static void main(String[] args) {
		AccountActivationApi api = new AccountActivationApi();

		System.out.println(" Going to make Request...");
		HttpRequestObject req = new HttpRequestObject(null, null,
				"/1.0/account_activiation?otp=21178&activation_key=bc4753ded8f24d09b13e2ec6ec24eba1&apiKey=a7ef2e069dd6a04a08ee1c3a23364db9c8b624bc738335dfaf7539d4e7d3e14d");
		HttpResponseObject res = api.execute(req);
		System.out.println(res.getResponse());
	}

}