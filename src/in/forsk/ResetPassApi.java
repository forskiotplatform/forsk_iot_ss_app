package in.forsk;

import in.forsk.utils.HttpUtils;

import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;
import java.util.UUID;

import org.joda.time.Period;
import org.json.JSONException;
import org.json.JSONObject;

import com.shephertz.app42.paas.customcode.Executor;
import com.shephertz.app42.paas.customcode.HttpRequestObject;
import com.shephertz.app42.paas.customcode.HttpResponseObject;
import com.shephertz.app42.paas.sdk.java.ServiceAPI;
import com.shephertz.app42.paas.sdk.java.storage.Storage;
import com.shephertz.app42.paas.sdk.java.storage.StorageService;
import com.shephertz.app42.paas.sdk.java.user.UserService;

public class ResetPassApi implements Executor {

	@Override
	public HttpResponseObject execute(HttpRequestObject req) {
		String body = req.getUri();
		try {

			if (body != null && body.length() > 0) {

				Map<String, String> map = HttpUtils.splitQuery(new URL("http://" + req.getUri()));

				ServiceAPI serviceAPI = new ServiceAPI("b0ae16872b9cec26214e6182c1022a28fa08bb1abc8a77f987fd91f0d85ca60d", "ab810bf7fca1ba3d75eaee6798f3b9b79eb2068193d0f6e4306c9b61d49a6765");

				UserService userService = serviceAPI.buildUserService();
				StorageService storageService = serviceAPI.buildStorageService();

				String forgetpass_key = map.get("forgetpass_key");
				String pass = map.get("pass");

				Storage storage_rule = storageService.findDocumentByKeyValue(GlobalConstant.DB_NAME, GlobalConstant.CollectionName.USER_PROPERTY, "forgetpass_key", forgetpass_key);
				ArrayList<Storage.JSONDocument> jsonDocList = storage_rule.getJsonDocList();

				if (jsonDocList.size() > 0) {

					JSONObject user_property = new JSONObject(jsonDocList.get(jsonDocList.size() - 1).getJsonDoc());

					String key_created_date = user_property.getString("key_created_date");
					

					DateFormat dateFormat = new SimpleDateFormat(GlobalConstant.DATE_FORMATE);

					Date date = new Date();

					Date key_date = dateFormat.parse(key_created_date);

					Period p = new Period(key_date.getTime(), date.getTime());
					int hours = p.getHours();

					if (hours < GlobalConstant.LINK_EXPIRE_OFFSET) {

						String phone = user_property.getString("phone"); 
						
						userService.resetUserPassword(phone);
						
						user_property.put("activation_key", UUID.randomUUID().toString().replaceAll("-", ""));
						user_property.put("forgetpass_key", UUID.randomUUID().toString().replaceAll("-", ""));

						storageService.updateDocumentByKeyValue(GlobalConstant.DB_NAME, GlobalConstant.CollectionName.USER_PROPERTY, "forgetpass_key", forgetpass_key, user_property);

						HttpResponseObject res = new HttpResponseObject(200, "Password reset successfully", null);
						return res;

					} else {
						// Link expire
						HttpResponseObject res = new HttpResponseObject(200, "forgetpass link expire", null);
						return res;
					}

				} else {
					HttpResponseObject res = new HttpResponseObject(200, "Invalid forgotpass link", null);
					return res;
				}

			} else {
				HttpResponseObject res = new HttpResponseObject(200, "Empty Body", null);
				return res;
			}

		} catch (Exception e) {
			e.printStackTrace();
			HttpResponseObject res = new HttpResponseObject(500, "{'message':'Internal Server'}", null);
			return res;
		}
	}

	public String getString(JSONObject jObject, String key) {
		try {
			return jObject.getString(key);
		} catch (JSONException e) {
			return "";
		}
	}
	
	public static void main(String[] args) {
		ResetPassApi api = new ResetPassApi();

		System.out.println(" Going to make Request...");
		HttpRequestObject req = new HttpRequestObject(null, null, "http://api-engine.shephertz.com/1.0/forget_pass?apiKey=a7ef2e069dd6a04a08ee1c3a23364db9c8b624bc738335dfaf7539d4e7d3e14d&forgetpass_key=ac7593e248584cabb5677a5e3b737f12");
		HttpResponseObject res = api.execute(req);
		System.out.println(res.getResponse());
	}

	
}