package in.forsk;

import in.forsk.utils.HttpUtils;

import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.json.JSONException;
import org.json.JSONObject;

import com.shephertz.app42.paas.customcode.Executor;
import com.shephertz.app42.paas.customcode.HttpRequestObject;
import com.shephertz.app42.paas.customcode.HttpResponseObject;
import com.shephertz.app42.paas.sdk.java.ServiceAPI;
import com.shephertz.app42.paas.sdk.java.email.EmailMIME;
import com.shephertz.app42.paas.sdk.java.storage.Storage;
import com.shephertz.app42.paas.sdk.java.storage.StorageService;
import com.shephertz.app42.paas.sdk.java.user.UserService;

public class SendForgetPassApi implements Executor {

	String host = "smtp.gmail.com";
	String emailId = "sorbh.kd@gmail.com";
	String password = "aazzaaddii";
	boolean isSSL = false;

	String sendSubject = "Forget Password Link";
	String sendMsg = "Your message";
	String senderEmailId = emailId;
	EmailMIME emailMime = EmailMIME.PLAIN_TEXT_MIME_TYPE;

	@Override
	public HttpResponseObject execute(HttpRequestObject req) {
		String body = req.getUri();
		try {

			if (body != null && body.length() > 0) {

				Map<String, String> map = HttpUtils.splitQuery(new URL("http://" + req.getUri()));

				ServiceAPI serviceAPI = new ServiceAPI("b0ae16872b9cec26214e6182c1022a28fa08bb1abc8a77f987fd91f0d85ca60d", "ab810bf7fca1ba3d75eaee6798f3b9b79eb2068193d0f6e4306c9b61d49a6765");

				UserService userService = serviceAPI.buildUserService();
				StorageService storageService = serviceAPI.buildStorageService();

				String email = map.get("email");
				String phone = map.get("phone");

				Storage storage_rule = storageService.findDocumentByKeyValue(GlobalConstant.DB_NAME, GlobalConstant.CollectionName.USER_PROPERTY, "phone", phone);
				ArrayList<Storage.JSONDocument> jsonDocList = storage_rule.getJsonDocList();

				String forgetpass_key = "";
				if (jsonDocList.size() > 0) {
					JSONObject user_property = new JSONObject(jsonDocList.get(jsonDocList.size() - 1).getJsonDoc());

					forgetpass_key = user_property.getString("forgetpass_key");

					DateFormat dateFormat = new SimpleDateFormat(GlobalConstant.DATE_FORMATE);
					Date date = new Date();

//					user_property.put("activation_key", UUID.randomUUID().toString().replaceAll("-", ""));
//					user_property.put("forgetpass_key", UUID.randomUUID().toString().replaceAll("-", ""));
					user_property.put("key_created_date", dateFormat.format(date));

					storageService.updateDocumentByKeyValue(GlobalConstant.DB_NAME, GlobalConstant.CollectionName.USER_PROPERTY, "phone", phone, user_property);
				}

				// Send email activation

				String activation_link = "http://api-engine.shephertz.com/1.0/forget_pass?apiKey=a7ef2e069dd6a04a08ee1c3a23364db9c8b624bc738335dfaf7539d4e7d3e14d&forgetpass_key=" + forgetpass_key;
				// Send email activation

				Properties props = new Properties();
				props.put("mail.smtp.auth", "true");
				props.put("mail.smtp.starttls.enable", "true");
				props.put("mail.smtp.host", host);
				props.put("mail.smtp.port", "587");

				// Get the Session object.
				Session session = Session.getInstance(props, new javax.mail.Authenticator() {
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(emailId, password);
					}
				});

				try {
					// Create a default MimeMessage object.
					Message message = new MimeMessage(session);

					// Set From: header field of the header.
					message.setFrom(new InternetAddress(emailId));

					// Set To: header field of the header.
					message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email));

					// Set Subject: header field
					message.setSubject(sendSubject);

					// Now set the actual message
					message.setText(activation_link);

					// Send message
					Transport.send(message);

					System.out.println("Sent message successfully....");

				} catch (MessagingException e) {
					throw new RuntimeException(e);
				}

				// Send SMS activation

				HttpResponseObject res = new HttpResponseObject(200, "forget pass link send", null);

				return res;
			} else {
				HttpResponseObject res = new HttpResponseObject(200, "Empty Body", null);
				return res;
			}

		} catch (Exception e) {
			e.printStackTrace();
			HttpResponseObject res = new HttpResponseObject(500, "{'message':'Internal Server'}", null);
			return res;
		}
	}

	public String getString(JSONObject jObject, String key) {
		try {
			return jObject.getString(key);
		} catch (JSONException e) {
			return "";
		}
	}

	public static void main(String[] args) {
		SendForgetPassApi api = new SendForgetPassApi();
		HttpRequestObject req = new HttpRequestObject(null, null,
				"/1.0/SendActivationApi?apiKey=a7ef2e069dd6a04a08ee1c3a23364db9c8b624bc738335dfaf7539d4e7d3e14d&email=er.sorbh@gmail.com&phone=8955088887");
		HttpResponseObject res = api.execute(req);
		System.out.println(res.getResponse());
	}
}