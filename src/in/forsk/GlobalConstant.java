package in.forsk;

public class GlobalConstant {

	//Url prefix
	public static final String DOMIAN = "http://api-engine.shephertz.com/";
	// Db name
	
	public static final String DB_NAME = "CARDPRO";

	
	public static final String DATE_FORMATE = "dd-MMM-yyyy HH:mm:ss";
	public static final String DEFAULT_PASS = "1234";
	
	public static final int LINK_EXPIRE_OFFSET = 6;
	

	// Collection Name
	public static class CollectionName {
		public static final String RULES = "RULES";
		public static final String IOT = "IOT";
		public static final String ACT = "ACT";
		public static final String USER_PROPERTY = "USER_PROPERTY";
		public static final String USER_ACTIVIATION = "USER_ACTIVATION";
	}

	// Url Name
	public static class ApiUrls {
		public static final String SMS_LOGS = "http://api-engine.shephertz.com/1.0/smslogs?apiKey=a7ef2e069dd6a04a08ee1c3a23364db9c8b624bc738335dfaf7539d4e7d3e14d&vendor=infobip";
		public static final String SMS_SEND = "http://api-engine.shephertz.com/1.0/smssend?apiKey=a7ef2e069dd6a04a08ee1c3a23364db9c8b624bc738335dfaf7539d4e7d3e14d&vendor=infobip";
		public static final String SMS_SEND_EXOTEL = "https://cardpro:0f054d4e87171f982833fceff8841058d271f6ae@twilix.exotel.in/v1/Accounts/cardpro/Sms/send";

	}
	
	public static class API{
        public final static String BASE_URL = "http://api-engine.shephertz.com/1.0/";
        public final static String REGISTRATION = BASE_URL+"registration?";
        public final static String SEND_ACTIVIATION = BASE_URL+"send_activiation?";
        public final static String ACCOUNT_ACTIVIATION = BASE_URL+"account_activiation?";
        public final static String LOGIN = BASE_URL+"login?";
       
    }

    public final static String API_KEY = "a7ef2e069dd6a04a08ee1c3a23364db9c8b624bc738335dfaf7539d4e7d3e14d";
}
