package in.forsk;

import in.forsk.utils.HttpUtils;

import java.net.URL;
import java.util.ArrayList;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.shephertz.app42.paas.customcode.Executor;
import com.shephertz.app42.paas.customcode.HttpRequestObject;
import com.shephertz.app42.paas.customcode.HttpResponseObject;
import com.shephertz.app42.paas.sdk.java.ServiceAPI;
import com.shephertz.app42.paas.sdk.java.storage.Storage;
import com.shephertz.app42.paas.sdk.java.storage.StorageService;

public class IOT_Endpoint implements Executor {
	HttpResponseObject res;

	@Override
	public HttpResponseObject execute(HttpRequestObject req) {
		String body = req.getBody();

		String result = "";
		try {

			Map<String, String> map = HttpUtils.splitQuery(new URL("http://" + req.getUri()));

			String phone_number = map.get("phone_number");

			ServiceAPI serviceAPI = new ServiceAPI("b0ae16872b9cec26214e6182c1022a28fa08bb1abc8a77f987fd91f0d85ca60d", "ab810bf7fca1ba3d75eaee6798f3b9b79eb2068193d0f6e4306c9b61d49a6765");
			StorageService storageService = serviceAPI.buildStorageService();

			try {
				Storage storage_rule = storageService.findDocumentByKeyValue(GlobalConstant.DB_NAME, GlobalConstant.CollectionName.ACT, "phone_number", phone_number);
				ArrayList<Storage.JSONDocument> jsonDocList = storage_rule.getJsonDocList();

				JSONObject json_noti = new JSONObject();

				if (jsonDocList.size() > 0) {
					JSONObject json_rule = new JSONObject(jsonDocList.get(jsonDocList.size() - 1).getJsonDoc());

					boolean one_bool = json_rule.getJSONArray("1").getBoolean(0);
					int one_int = json_rule.getJSONArray("1").getInt(1);

					boolean two_bool = json_rule.getJSONArray("2").getBoolean(0);
					int two_int = json_rule.getJSONArray("2").getInt(1);

					boolean three_bool = json_rule.getJSONArray("3").getBoolean(0);
					int three_int = json_rule.getJSONArray("3").getInt(1);

					result = "" + one_bool + "," + one_int + "," + two_bool + "," + two_int + "," + three_bool + "," + three_int;
					res = new HttpResponseObject(200, result, null);
				} else {

				}
			} catch (Exception e) {
				res = new HttpResponseObject(200, "NO VALUE", null);
			}

			return res;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			HttpResponseObject res = new HttpResponseObject(404, "{'message':'Invalid Input Type'}", null);
			return res;
		}

	}

}
