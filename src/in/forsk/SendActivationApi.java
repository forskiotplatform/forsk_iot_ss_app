package in.forsk;

import in.forsk.utils.HttpUtils;
import in.forsk.utils.MainGun;
import in.forsk.utils.PlivoUtils;
import in.forsk.utils.ReponseUtils;
import in.forsk.utils.UrlShortner;

import java.awt.PageAttributes.MediaType;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.json.JSONException;
import org.json.JSONObject;

import com.shephertz.app42.paas.customcode.Executor;
import com.shephertz.app42.paas.customcode.HttpRequestObject;
import com.shephertz.app42.paas.customcode.HttpResponseObject;
import com.shephertz.app42.paas.sdk.java.ServiceAPI;
import com.shephertz.app42.paas.sdk.java.email.EmailMIME;
import com.shephertz.app42.paas.sdk.java.email.EmailService;
import com.shephertz.app42.paas.sdk.java.storage.Storage;
import com.shephertz.app42.paas.sdk.java.storage.StorageService;
import com.shephertz.app42.paas.sdk.java.user.UserService;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;
import com.sun.jersey.core.util.MultivaluedMapImpl;

public class SendActivationApi implements Executor {

	HttpResponseObject res;

	@Override
	public HttpResponseObject execute(HttpRequestObject req) {
		String body = req.getUri();
		try {

			if (body != null && body.length() > 0) {

				Map<String, String> map = HttpUtils.splitQuery(new URL("http://api-engine.shephertz.com/" + req.getUri()));

				ServiceAPI serviceAPI = new ServiceAPI("b0ae16872b9cec26214e6182c1022a28fa08bb1abc8a77f987fd91f0d85ca60d", "ab810bf7fca1ba3d75eaee6798f3b9b79eb2068193d0f6e4306c9b61d49a6765");

				StorageService storageService = serviceAPI.buildStorageService();

				String email = map.get("email");

				String phone = map.get("phone");

				String response_message = "";

				if (email != null && email.length() > 0) {
					Storage storage_rule = storageService.findDocumentByKeyValue(GlobalConstant.DB_NAME, GlobalConstant.CollectionName.USER_PROPERTY, "email", email);
					ArrayList<Storage.JSONDocument> jsonDocList = storage_rule.getJsonDocList();

					String activation_key = "";
					if (jsonDocList.size() > 0) {
						JSONObject user_property = new JSONObject(jsonDocList.get(jsonDocList.size() - 1).getJsonDoc());

						activation_key = user_property.getString("activation_key");

						DateFormat dateFormat = new SimpleDateFormat(GlobalConstant.DATE_FORMATE);
						Date date = new Date();

						user_property.put("key_created_date", dateFormat.format(date));

						String activation_link = "http://api-engine.shephertz.com/1.0/account_activiation?apiKey=a7ef2e069dd6a04a08ee1c3a23364db9c8b624bc738335dfaf7539d4e7d3e14d&activation_key="
								+ activation_key + "&activation_type=" + 1
						// + "&otp=" + OTP
						;

						String url = UrlShortner.shorten(activation_link);

						PlivoUtils.sendSMS(phone, "" + url);
						// MainGun.sendEmail(email, activation_link);
						//
						// System.out.println(clientResponse.getResponseDate());

						// Send SMS activation

						storageService.updateDocumentByKeyValue(GlobalConstant.DB_NAME, GlobalConstant.CollectionName.USER_PROPERTY, "phone", phone, user_property);

						response_message = "activation mail send";
					}
				}

				if (phone != null && phone.length() > 0) {
					Storage storage_rule = storageService.findDocumentByKeyValue(GlobalConstant.DB_NAME, GlobalConstant.CollectionName.USER_PROPERTY, "phone", phone);
					ArrayList<Storage.JSONDocument> jsonDocList = storage_rule.getJsonDocList();

					String OTP = "";
					if (jsonDocList.size() > 0) {
						JSONObject user_property = new JSONObject(jsonDocList.get(jsonDocList.size() - 1).getJsonDoc());

						OTP = user_property.getString("otp");

						DateFormat dateFormat = new SimpleDateFormat(GlobalConstant.DATE_FORMATE);
						Date date = new Date();

						user_property.put("key_created_date", dateFormat.format(date));

						PlivoUtils.sendSMS(phone, "" + OTP);

						storageService.updateDocumentByKeyValue(GlobalConstant.DB_NAME, GlobalConstant.CollectionName.USER_PROPERTY, "phone", phone, user_property);

						response_message = "/OTP send";
					}

					res = new HttpResponseObject(200, ReponseUtils.getSuccessResponse(response_message), null);
				}

				return res;
			} else {
				HttpResponseObject res = new HttpResponseObject(200, "Empty Body", null);
				return res;
			}

		} catch (Exception e) {
			e.printStackTrace();
			HttpResponseObject res = new HttpResponseObject(500, "{'message':'Internal Server'}", null);
			return res;
		}
	}

	public String getString(JSONObject jObject, String key) {
		try {
			return jObject.getString(key);
		} catch (JSONException e) {
			return "";
		}
	}

	public static void main(String[] args) {
		SendActivationApi api = new SendActivationApi();
		HttpRequestObject req = new HttpRequestObject(null, null,
				"1.0/send_activiation?phone=8955088887&email=er.sorbh%40gmail.com&apiKey=a7ef2e069dd6a04a08ee1c3a23364db9c8b624bc738335dfaf7539d4e7d3e14d");
		HttpResponseObject res = api.execute(req);
		System.out.println(res.getResponse());
	}

}