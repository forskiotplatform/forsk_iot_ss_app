package in.forsk.utils;

import org.json.JSONException;
import org.json.JSONObject;

public class ReponseUtils {
	
	public static class ErrorCode{
		public static int 
	}

	public static String getSuccessResponse(String message, JSONObject object) {
		JSONObject reponse = new JSONObject();
		try {

			reponse.put("error", 0);
			reponse.put("message", message);
			reponse.put("data", object.toString());

			return reponse.toString();
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return reponse.toString();
	}

	public static String getSuccessResponse(String message) {
		JSONObject reponse = new JSONObject();
		try {

			reponse.put("error", 0);
			reponse.put("message", message);

			return reponse.toString();
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return reponse.toString();
	}

	public static String getErrorResponse(String message) {
		JSONObject reponse = new JSONObject();
		try {

			reponse.put("error", 1);
			reponse.put("message", message);
			return reponse.toString();
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return reponse.toString();
	}
}
