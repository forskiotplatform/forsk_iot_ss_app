package in.forsk.utils;

import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;
import com.sun.jersey.core.util.MultivaluedMapImpl;

public class MainGun {

	private final static String DOMIAN_NAME = "sandbox093651ba584149518f817eb715215ba6.mailgun.org";
	private final static String API_KEY = "key-7765a4c4528862f0fbc13c59bf1b9348";

	public static ClientResponse sendEmail(String to, String message) {
		Client client = Client.create();
		client.addFilter(new HTTPBasicAuthFilter("api", API_KEY));
		WebResource webResource = client.resource("https://api.mailgun.net/v3/" + DOMIAN_NAME + "/messages");
		
		MultivaluedMapImpl formData = new MultivaluedMapImpl();
		
		formData.add("from", "no-reply@forsk.in");
		formData.add("to", to);
		formData.add("subject", "Activation Link");
		formData.add("text", message);
//		formData.add("html", email.getMessage());

		return webResource.type(MediaType.APPLICATION_FORM_URLENCODED).post(ClientResponse.class, formData);
	}
}
