package in.forsk.utils;

import java.util.HashMap;
import java.util.LinkedHashMap;

import com.plivo.helper.api.client.RestAPI;
import com.plivo.helper.api.response.message.MessageResponse;
import com.plivo.helper.exception.PlivoException;

public class PlivoUtils {
	private static final String TAG = PlivoUtils.class.getSimpleName();

	public static void sendSMS(String to, String message) throws Exception {
//		HashMap<String, String> params = new HashMap<>();
//		// params.put("src", "");
		to = to.startsWith("" + 91) ? to : "91" + to;
//
//		params.put("src", "+14042021965");
//		params.put("dst", to);
//		params.put("text", message);
//
//		String url = "https://api.plivo.com/v1/Account/MAMTI2NJDMMJQYOWMXMW/Message/";
//
//		String respose = HttpUtils.sendPost(url, params);
//
//		System.out.println(respose);
		
		String authId = "MAMTI2NJDMMJQYOWMXMW";
        String authToken = "Mjc4MzhhZDcxMWJiODgyNjRkNTRkMjg5ODE4ZGIy";
        RestAPI api = new RestAPI(authId, authToken, "v1");

        LinkedHashMap<String, String> parameters = new LinkedHashMap<String, String>();
        parameters.put("src", "14042021965"); // Sender's phone number with country code
        parameters.put("dst", to); // Receiver's phone number with country code
        parameters.put("text", message); // Your SMS text message
        // Send Unicode text
        //parameters.put("text", "こんにちは、元気ですか？"); // Your SMS text message - Japanese
        //parameters.put("text", "Ce est texte généré aléatoirement"); // Your SMS text message - French
//        parameters.put("url", "http://example.com/report/"); // The URL to which with the status of the message is sent
//        parameters.put("method", "GET"); // The method used to call the url

        try {
            // Send the message
            MessageResponse msgResponse = api.sendMessage(parameters);

            // Print the response
            System.out.println(msgResponse);
            // Print the Api ID
            System.out.println("Api ID : " + msgResponse.apiId);
            // Print the Response Message
            System.out.println("Message : " + msgResponse.message);

            if (msgResponse.serverCode == 202) {
                // Print the Message UUID
                System.out.println("Message UUID : " + msgResponse.messageUuids.get(0).toString());
            } else {
                System.out.println(msgResponse.error);
            }
        } catch (PlivoException e) {
            System.out.println(e.getLocalizedMessage());
        }
	}

}
