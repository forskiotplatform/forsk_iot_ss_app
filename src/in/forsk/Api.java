package in.forsk;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.shephertz.app42.paas.customcode.Executor;
import com.shephertz.app42.paas.customcode.HttpRequestObject;
import com.shephertz.app42.paas.customcode.HttpResponseObject;
import com.shephertz.app42.paas.sdk.java.ServiceAPI;
import com.shephertz.app42.paas.sdk.java.push.PushNotificationService;
import com.shephertz.app42.paas.sdk.java.storage.Storage;
import com.shephertz.app42.paas.sdk.java.storage.StorageService;

public class Api implements Executor {

	@Override
	public HttpResponseObject execute(HttpRequestObject req) {
		String body = req.getBody();
		try {

			if (body != null && body.length() > 0) {
				System.out.println(" Body Recieved :  " + body);

				JSONObject jsonObj = new JSONObject(body);

				JSONObject responseObj = new JSONObject();

				final String phone_number = jsonObj.getString("phone_number");

				final String Temp = getString(jsonObj, "Temp");
				final String POWER = getString(jsonObj, "POWER");
				final String HUM = getString(jsonObj, "HUM");
				final String LDR = getString(jsonObj, "LDR");
				final String PIR = getString(jsonObj, "PIR");
				final String DOOR = getString(jsonObj, "DOOR");

				// DateFormat dateFormat = new
				// SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
				DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy       HH:mm:ss");
				Date date = new Date();

				jsonObj.put("date", dateFormat.format(date));

				ServiceAPI serviceAPI = new ServiceAPI("b0ae16872b9cec26214e6182c1022a28fa08bb1abc8a77f987fd91f0d85ca60d", "ab810bf7fca1ba3d75eaee6798f3b9b79eb2068193d0f6e4306c9b61d49a6765");
				StorageService storageService = serviceAPI.buildStorageService();

				PushNotificationService pushNotificationService = serviceAPI.buildPushNotificationService();

				try {
					Storage storage_rule = storageService.findDocumentByKeyValue(GlobalConstant.DB_NAME, GlobalConstant.CollectionName.RULES, "phone_number", phone_number);
					ArrayList<Storage.JSONDocument> jsonDocList = storage_rule.getJsonDocList();

					JSONObject json_noti = new JSONObject();

					if (jsonDocList.size() > 0) {
						JSONObject json_rule = new JSONObject(jsonDocList.get(jsonDocList.size() - 1).getJsonDoc());

						int temp_max = json_rule.getJSONArray("Temp").getInt(0);
						int temp_min = json_rule.getJSONArray("Temp").getInt(1);

						String power_rule = json_rule.getJSONArray("POWER").getString(0);

						int hum_max = json_rule.getJSONArray("HUM").getInt(0);
						int hum_min = json_rule.getJSONArray("HUM").getInt(1);

						int ldr_max = json_rule.getJSONArray("LDR").getInt(0);
						int ldr_min = json_rule.getJSONArray("LDR").getInt(1);

						String pir_rule = json_rule.getJSONArray("PIR").getString(0);

						String door_rule = json_rule.getJSONArray("DOOR").getString(0);

						JSONArray alert_for = new JSONArray();
						JSONArray messages = new JSONArray();

						if (Temp.length() > 0 && (temp_min >= Double.parseDouble(Temp) || Double.parseDouble(Temp) >= temp_max)) {
							alert_for.put("Temp");
							messages.put("Temp Alert - "+Temp);
						}

						if (HUM.length() > 0 && (hum_min >= Double.parseDouble(HUM) || Double.parseDouble(HUM) >= hum_max)) {
							alert_for.put("HUM");
							messages.put("HUM Alert - " +HUM);
						}

						if (LDR.length() > 0 && (ldr_min >= Double.parseDouble(LDR) || Double.parseDouble(LDR) >= ldr_max)) {
							alert_for.put("LDR");
							messages.put("LDR Alert - "+LDR);
						}

						if (POWER.length() > 0 && POWER.equalsIgnoreCase("" + power_rule)) {
							alert_for.put("POWER");
							messages.put("POWER Alert - "+POWER);
						}

						if (PIR.length() > 0 && PIR.equalsIgnoreCase("" + pir_rule)) {
							alert_for.put("PIR");
							messages.put("PIR Alert - "+PIR );
						}

						if (DOOR.length() > 0 && DOOR.equalsIgnoreCase("" + door_rule)) {
							alert_for.put("DOOR");
							messages.put("DOOR Alert - "+DOOR);
						}

						if (alert_for.length() > 0 && messages.length() > 0) {
							json_noti.put("alert_for", alert_for);
							json_noti.put("messages", messages);

							pushNotificationService.sendPushMessageToUser(phone_number, json_noti.toString());
						}

					} else {

					}
				} catch (Exception e) {
				}

				Storage storage = storageService.insertJSONDocument(GlobalConstant.DB_NAME, GlobalConstant.CollectionName.IOT, jsonObj);

				HttpResponseObject res = new HttpResponseObject(200, "OK", null);

				return res;
			} else {
				HttpResponseObject res = new HttpResponseObject(200, "Empty Body", null);
				return res;
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			HttpResponseObject res = new HttpResponseObject(404, "{'message':'Invalid Input Type'}", null);
			return res;
		} catch (Exception e) {
			e.printStackTrace();
			HttpResponseObject res = new HttpResponseObject(500, "{'message':'Internal Server'}", null);
			return res;
		}
	}

	public String getString(JSONObject jObject, String key) {
		try {
			return jObject.getString(key);
		} catch (JSONException e) {
			return "";
		}
	}

	public static void main(String[] args) {
		Api api = new Api();
		JSONObject jsonObj = null;
		try {
			jsonObj = new JSONObject("{\"phone_number\":\"9414136621\",\"date\":\"15-Apr-2016       15:41:26\",\"Temp\":\"26.88\",\"LDR\":\"194.04\",\"HUM\":\"16.00\"}");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println(" Going to make Request...");
		HttpRequestObject req = new HttpRequestObject(null, jsonObj.toString(), null);
		HttpResponseObject res = api.execute(req);
		System.out.println(res.getResponse());
	}

}
