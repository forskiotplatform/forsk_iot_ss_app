package in.forsk;

import in.forsk.utils.HttpUtils;
import in.forsk.utils.ReponseUtils;

import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.UUID;

import org.json.JSONException;
import org.json.JSONObject;

import com.shephertz.app42.paas.customcode.Executor;
import com.shephertz.app42.paas.customcode.HttpRequestObject;
import com.shephertz.app42.paas.customcode.HttpResponseObject;
import com.shephertz.app42.paas.sdk.java.ServiceAPI;
import com.shephertz.app42.paas.sdk.java.storage.StorageService;
import com.shephertz.app42.paas.sdk.java.user.UserService;

public class RegistrationApi implements Executor {

	@Override
	public HttpResponseObject execute(HttpRequestObject req) {
		String body = req.getUri();
		try {

			if (body != null && body.length() > 0) {

				Map<String, String> map = HttpUtils.splitQuery(new URL("http://api-engine.shephertz.com/" + req.getUri()));

				ServiceAPI serviceAPI = new ServiceAPI("b0ae16872b9cec26214e6182c1022a28fa08bb1abc8a77f987fd91f0d85ca60d", "ab810bf7fca1ba3d75eaee6798f3b9b79eb2068193d0f6e4306c9b61d49a6765");

				UserService userService = serviceAPI.buildUserService();
				StorageService storageService = serviceAPI.buildStorageService();

				String update = map.get("update");
				
				String email = map.get("email");
				String phone = map.get("phone");
				String pass = map.get("pass");

				DateFormat dateFormat = new SimpleDateFormat(GlobalConstant.DATE_FORMATE);
				Date date = new Date();

				JSONObject user_property = new JSONObject();
				
				user_property.put("phone", phone);
				
				if(email != null && email.length() >0){
					user_property.put("email", email);
				}else {
					user_property.put("email", "");
				}
				
				if(pass != null && pass.length() >0){
					user_property.put("pass", pass);
				}else {
					user_property.put("pass", pass);
				}

				

				userService.createUser(phone, GlobalConstant.DEFAULT_PASS, email);

				if (update.equalsIgnoreCase("false")) {
					
					user_property.put("activation_key", UUID.randomUUID().toString().replaceAll("-", ""));
					user_property.put("is_email_verified", 0);
					user_property.put("email_verification_time", 0);

					user_property.put("otp", "" + gen());
					user_property.put("is_mobile_verified", 0);
					user_property.put("mobile_verification_time", 0);

					user_property.put("key_created_date", dateFormat.format(date));

					user_property.put("forgetpass_key", UUID.randomUUID().toString().replaceAll("-", ""));

					user_property.put("created_date", dateFormat.format(date));
					
					storageService.insertJSONDocument(GlobalConstant.DB_NAME, GlobalConstant.CollectionName.USER_PROPERTY, user_property);
				} else {
					storageService.updateDocumentByKeyValue(GlobalConstant.DB_NAME, GlobalConstant.CollectionName.USER_PROPERTY, "phone", phone, user_property);
				}
				// Send Activation email and sms api
				// HashMap<String, String> params = new HashMap<String,
				// String>();
				// params.put("apiKey", GlobalConstant.API_KEY);
				// params.put("email", email);
				// params.put("phone", phone);
				//
				// String url = GlobalConstant.API.SEND_ACTIVIATION +
				// HttpUtils.getPostDataString(params);
				// HttpUtils.sendGet(url);

				HttpResponseObject res = new HttpResponseObject(200, ReponseUtils.getSuccessResponse("User Identified", user_property), null);

				return res;
			} else {
				HttpResponseObject res = new HttpResponseObject(200, "Empty Body", null);
				return res;
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			HttpResponseObject res = new HttpResponseObject(404, "{'message':'Invalid Input Type'}", null);
			return res;
		} catch (Exception e) {
			e.printStackTrace();
			HttpResponseObject res = new HttpResponseObject(500, e.getMessage(), null);
			return res;
		}
	}

	public String getString(JSONObject jObject, String key) {
		try {
			return jObject.getString(key);
		} catch (JSONException e) {
			return "";
		}
	}

	public int gen() {
		Random r = new Random(System.currentTimeMillis());
		return ((1 + r.nextInt(2)) * 10000 + r.nextInt(10000));
	}

	public static void main(String[] args) {
		RegistrationApi api = new RegistrationApi();
		HttpRequestObject req = new HttpRequestObject(null, null,
				"/1.0/registration?apiKey=a7ef2e069dd6a04a08ee1c3a23364db9c8b624bc738335dfaf7539d4e7d3e14d&email=er.sorbh@gmail.com&phone=8955088887&pass=1234");
		HttpResponseObject res = api.execute(req);
		System.out.println(res.getResponse());
	}
}