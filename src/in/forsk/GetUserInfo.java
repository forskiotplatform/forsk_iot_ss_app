package in.forsk;

import in.forsk.utils.HttpUtils;
import in.forsk.utils.ReponseUtils;

import java.net.URL;
import java.util.ArrayList;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import com.shephertz.app42.paas.customcode.Executor;
import com.shephertz.app42.paas.customcode.HttpRequestObject;
import com.shephertz.app42.paas.customcode.HttpResponseObject;
import com.shephertz.app42.paas.sdk.java.ServiceAPI;
import com.shephertz.app42.paas.sdk.java.storage.Query;
import com.shephertz.app42.paas.sdk.java.storage.QueryBuilder;
import com.shephertz.app42.paas.sdk.java.storage.Storage;
import com.shephertz.app42.paas.sdk.java.storage.StorageService;
import com.shephertz.app42.paas.sdk.java.storage.QueryBuilder.Operator;
import com.shephertz.app42.paas.sdk.java.user.UserService;

public class GetUserInfo implements Executor {

	HttpResponseObject res;

	@Override
	public HttpResponseObject execute(HttpRequestObject req) {
		String body = req.getUri();

		JSONObject response = new JSONObject();
		try {

			if (body != null && body.length() > 0) {

				Map<String, String> map = HttpUtils.splitQuery(new URL("http://api-engine.shephertz.com/" + req.getUri()));

				ServiceAPI serviceAPI = new ServiceAPI("b0ae16872b9cec26214e6182c1022a28fa08bb1abc8a77f987fd91f0d85ca60d", "ab810bf7fca1ba3d75eaee6798f3b9b79eb2068193d0f6e4306c9b61d49a6765");

				UserService userService = serviceAPI.buildUserService();
				StorageService storageService = serviceAPI.buildStorageService();

				String phone = map.get("phone");

				final String key = "phone";

				try {
					Storage storage_rule = storageService.findDocumentByKeyValue(GlobalConstant.DB_NAME, GlobalConstant.CollectionName.USER_PROPERTY, key, phone);
					ArrayList<Storage.JSONDocument> jsonDocList = storage_rule.getJsonDocList();

					if (jsonDocList.size() > 0) {

						JSONObject user_property = new JSONObject(jsonDocList.get(jsonDocList.size() - 1).getJsonDoc());

						res = new HttpResponseObject(200, ReponseUtils.getSuccessResponse("User Identified", user_property), null);

					} else {
						res = new HttpResponseObject(200, ReponseUtils.getErrorResponse("Not valid user"), null);
					}

				} catch (Exception e) {
					res = new HttpResponseObject(200, ReponseUtils.getErrorResponse("New user, Please Signup first"), null);
				}

				return res;
			} else {
				res = new HttpResponseObject(200, "Empty Body", null);
				return res;
			}

		} catch (Exception e) {
			e.printStackTrace();
			res = new HttpResponseObject(500, e.getMessage(), null);
			return res;
		}
	}

	public String getString(JSONObject jObject, String key) {
		try {
			return jObject.getString(key);
		} catch (JSONException e) {
			return "";
		}
	}

	public static void main(String[] args) {
		RegistrationApi api = new RegistrationApi();
		JSONObject jsonObj = null;
		try {
			jsonObj = new JSONObject("{\"email\":\"er.sorbh@gmail.com\",\"phone\":\"8955088887\",\"pass\":\"1234\"}");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println(" Going to make Request...");
		HttpRequestObject req = new HttpRequestObject(null, jsonObj.toString(), null);
		HttpResponseObject res = api.execute(req);
		System.out.println(res.getResponse());
	}
}