package in.forsk;

import in.forsk.utils.HttpUtils;
import in.forsk.utils.ReponseUtils;

import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;
import org.omg.CORBA.RepositoryIdHelper;

import com.shephertz.app42.paas.customcode.Executor;
import com.shephertz.app42.paas.customcode.HttpRequestObject;
import com.shephertz.app42.paas.customcode.HttpResponseObject;
import com.shephertz.app42.paas.sdk.java.ServiceAPI;
import com.shephertz.app42.paas.sdk.java.storage.Query;
import com.shephertz.app42.paas.sdk.java.storage.QueryBuilder;
import com.shephertz.app42.paas.sdk.java.storage.Storage;
import com.shephertz.app42.paas.sdk.java.storage.StorageService;
import com.shephertz.app42.paas.sdk.java.storage.QueryBuilder.Operator;
import com.shephertz.app42.paas.sdk.java.user.UserService;

public class LoginApi implements Executor {

	HttpResponseObject res;

	@Override
	public HttpResponseObject execute(HttpRequestObject req) {
		String body = req.getUri();

		JSONObject response = new JSONObject();
		try {

			if (body != null && body.length() > 0) {

				Map<String, String> map = HttpUtils.splitQuery(new URL("http://api-engine.shephertz.com/" + req.getUri()));

				ServiceAPI serviceAPI = new ServiceAPI("b0ae16872b9cec26214e6182c1022a28fa08bb1abc8a77f987fd91f0d85ca60d", "ab810bf7fca1ba3d75eaee6798f3b9b79eb2068193d0f6e4306c9b61d49a6765");

				UserService userService = serviceAPI.buildUserService();
				StorageService storageService = serviceAPI.buildStorageService();

				String phone = map.get("phone");
				String pass = map.get("pass");

				final String key = "phone";
				final String value = phone;
				final String key1 = "pass";
				final String value1 = pass;

				Query q1 = QueryBuilder.build(key, value, Operator.EQUALS);
				Query q2 = QueryBuilder.build(key1, value1, Operator.EQUALS);
				Query query = QueryBuilder.compoundOperator(q1, Operator.AND, q2);

				try {
					Storage storage_rule = storageService.findDocumentsByQuery(GlobalConstant.DB_NAME, GlobalConstant.CollectionName.USER_PROPERTY, query);
					ArrayList<Storage.JSONDocument> jsonDocList = storage_rule.getJsonDocList();

					if (jsonDocList.size() > 0) {

						JSONObject user_property = new JSONObject(jsonDocList.get(jsonDocList.size() - 1).getJsonDoc());

						String is_email_verified = user_property.getString("is_email_verified");
						String is_mobile_verified = user_property.getString("is_mobile_verified");

						if (is_email_verified.equalsIgnoreCase("1") && is_mobile_verified.equalsIgnoreCase("1")) {
							res = new HttpResponseObject(200, ReponseUtils.getSuccessResponse("User Authenticated"), null);
						} else if (is_email_verified.equalsIgnoreCase("0")) {
							res = new HttpResponseObject(200, ReponseUtils.getErrorResponse("Mail is not varified"), null);
						} else if (is_mobile_verified.equalsIgnoreCase("0")) {
							res = new HttpResponseObject(200, ReponseUtils.getErrorResponse("mobile is not varified"), null);
						}

					} else {
						res = new HttpResponseObject(200, ReponseUtils.getErrorResponse("New user, Please Signup first"), null);
					}

				} catch (Exception e) {
					res = new HttpResponseObject(200, ReponseUtils.getErrorResponse("New user, Please Signup first"), null);
				}

				return res;
			} else {
				res = new HttpResponseObject(200, "Empty Body", null);
				return res;
			}

		} catch (Exception e) {
			e.printStackTrace();
			res = new HttpResponseObject(500, e.getMessage(), null);
			return res;
		}
	}

	public String getString(JSONObject jObject, String key) {
		try {
			return jObject.getString(key);
		} catch (JSONException e) {
			return "";
		}
	}

	public static void main(String[] args) {
		RegistrationApi api = new RegistrationApi();
		JSONObject jsonObj = null;
		try {
			jsonObj = new JSONObject("{\"email\":\"er.sorbh@gmail.com\",\"phone\":\"8955088887\",\"pass\":\"1234\"}");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println(" Going to make Request...");
		HttpRequestObject req = new HttpRequestObject(null, jsonObj.toString(), null);
		HttpResponseObject res = api.execute(req);
		System.out.println(res.getResponse());
	}
}